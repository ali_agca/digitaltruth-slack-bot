#! /usr/bin/env python

import datetime
import os
import random

import cv2
import numpy as np
import requests

import dlib

from .utils import face_swap2


class SwapFaces(object):
    def __init__(self):
        print("[INFO] loading facial landmark predictor...")
        self.model = "libs/faceswap/shape_predictor_68_face_landmarks.dat"
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(self.model)

    def url_to_image(self, url, mime):
        # download the image, convert it to a NumPy array, and then read
        # it into OpenCV format
        headers = {
            'Authorization': 'Bearer %s' % os.environ.get('SLACKBOT_TOKEN')
        }
        resp = requests.get(url, headers=headers)
        ext = None
        
        if 'jpeg' in mime:
            ext = 'jpg'

        if 'png' in mime:
            ext = 'png'

        if not ext:
            return None

        fn = self.random_filename(ext=ext)
        with open(fn, 'wb') as tmp_img:
            tmp_img.write(resp.content)
        return fn

    def random_filename(self, ext='jpg'):
        now = datetime.datetime.now().__str__().replace(' ', '-')
        rnd = str(random.randint(1000, 99999999))
        return '/tmp/%s.%s.%s' % (now, rnd, ext)

    def swap(self, source, destination, mime):
        source_img = cv2.imread(source)
        temp = self.url_to_image(destination, mime)
        if not temp:
            return
        dest_img = cv2.imread(temp)
        os.unlink(temp)

        output1, output2 = face_swap2(dest_img, source_img, self.detector, self.predictor)
        if output2 is not None:
            fn = self.random_filename()
            cv2.imwrite(fn, output2)
            return fn
