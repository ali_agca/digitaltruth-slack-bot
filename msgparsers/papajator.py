import os

from base import BaseMessageParser
from libs.faceswap.swap_api import SwapFaces


class Papajator(BaseMessageParser):
    def __init__(self):
        self.swapper = SwapFaces()
        self.slack_cli = None
    
    def handle_message(self, event, bot_id):
        if event.get('user','') == bot_id:
            return

        if 'files' not in event:
            return
        
        url = event['files'][0]['url_private_download']
        mime = event['files'][0]['mimetype']
        channel = event['channel']

        swapped = self.swapper.swap('libs/faceswap/data/papaj.jpg', url, mime)
        if swapped:
            resp = self.slack_cli.api_call(
                "files.upload",
                channels=[channel],
                file=open(swapped, 'rb'),
                title="hehehe"
            )
            os.unlink(swapped)
