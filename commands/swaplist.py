import os

from base import BaseCommand


class SwapList(BaseCommand):
    command = 'faces'

    def handle_command(self, command, channel):
        files = os.listdir('libs/faceswap/data')
        return ', '.join([fn.split('.')[0] for fn in files])
