from random import choice
from base import BaseCommand

class SimpleAnswer(BaseCommand):
    command = 'czy'

    answers = [
        'być może',
        'nie wiem',
        'a jak pan jezus powiedział?',
        'może',
        'jeden rabin powie tak, inny powie nie',
        'ciężko powiedzieć',
        'dupsko xD'
    ]

    def handle_command(self, command, channel):
        if 'stare prawo' in command:
            return 'NARODOWY SOCJALIZM!'
        return choice(self.answers)

