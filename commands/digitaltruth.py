
from base import BaseCommand
import requests
from bs4 import BeautifulSoup


def format_time_cell(value):
    try:
        delim = '-'
        if '+' in value:
            delim = '+'
        times = value.split(delim)
        kurwa = []
        for ts in times:
            kurwa.append(('%02.2f' % float(ts)).rjust(5, '0'))
        return delim.join(kurwa)
    except ValueError:
        return value

class DigitaltruthCommand(BaseCommand):
    command = 'times for'
    no_results = [
        'Chuj mi w ryj',
        'Pierdole',
        'Gówno',
        'Sranie do ryja',
        'Wymocz se to w sikach xD',
        'Nasraj sobie do ryja',
        'Poszukaj w internecie xD',
        'Skontaktuj się ze sprzedawcą i nasraj se do mordy xD',
        'Pomódl się do Jana Wywoływacza II, wielkiego pedofila',
        'Ja bym polecał caffenol'
    ]

    def get_note_text(self, url):
        resp = requests.get(url)
        doc = BeautifulSoup(resp.content, "lxml")
        table = doc.find_all('table', {'class': 'notenote'})
        rows = table[0].find_all('tr')
        ret_arr = []
        for row in rows:
            for td in row.find_all('td'):
                txt = td.text.strip()
                if len(txt) > 2:
                    ret_arr.append(txt)
        return ', '.join(ret_arr)

    def get_notes(self, cell):
        finds = cell.find_all('a')
        if not finds:
            return ''
        else:
            return self.get_note_text('https://digitaltruth.com/' + finds[0]['href'])

    def digitaltruth_search(self, query):
        q_arr = query.split(' in ')
        film = q_arr[0].strip()
        dev = q_arr[1].strip()
        params = {
            'Film': '%' + film + '%',
            'Developer': '%' + dev + '%',
            'mdc': 'Search',
            'TempUnits': 'C',
            'TimeUnits': 'D'
        }
        resp = requests.get('https://digitaltruth.com/devchart.php', params=params)

        try:
            doc = BeautifulSoup(resp.content, "lxml")
            tab = doc.find_all('table', {'class': "mdctable"})[0]

            resp = []
            rows = tab.find_all('tr')
        except IndexError:
            return []

        for row in rows[1:]:
            cells = row.find_all('td')
            dat = {
                'film': cells[0].text,
                'dev': cells[1].text,
                'dil': cells[2].text,
                'EI': cells[3].text,
                'time': cells[4].text,
                'temp': cells[7].text,
                'notes': self.get_notes(cells[8]),
                'time_fmt': format_time_cell(cells[4].text)
            }
            resp.append(dat)
        return resp

    def handle_command(self, command, channel):
        """
        Executes bot command if the command is known
        """

        response = None
        command = command.replace(self.command + ' ', '')

        try:
            response = ""
            resp_data = self.digitaltruth_search(command)

            if not resp_data:
                response = "Brak danych w badzie cyfrowej gównoprawdy. %s" % choice(self.no_results)
            else:
                for item in resp_data:
                    response += "*%(film)s in %(dev)s*\t(%(dil)s) at %(EI)s:\t:clock3: *%(time_fmt)s*\t:thermometer: %(temp)s\t %(notes)s\n" % item
        except Exception as e:
            response = "Spierdolilo sie cos %s" % e
        return response
