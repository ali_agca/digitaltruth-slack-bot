import os
import re
import sys
import time

from slackclient import SlackClient

RTM_READ_DELAY = 0.1

# constants
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"


def parse_bot_commands(slack_events, bot_id):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and "subtype" not in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == bot_id:
                return message, event["channel"]
    return None, None


def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)


class BaseCommand(object):
    command = ''

    def handle_command(self, command, channel):
        raise NotImplementedError()


class BaseMessageParser(object):
    def handle_message():
        pass


def main_loop(commands, parsers):
    if os.environ.get('SLACKBOT_TOKEN', None) is None:
        print("SLACKBOT_TOKEN Env variable not set.")
        sys.exit(1)

    default_response = "Żuj chuj śmieciu jebany xD"

    slack_client = SlackClient(os.environ['SLACKBOT_TOKEN'])
    bot_id = None

    if slack_client.rtm_connect(with_team_state=False):
        print("Starter Bot connected and running!")
        bot_id = slack_client.api_call("auth.test")["user_id"]

        while True:
            events = slack_client.rtm_read()
            for parser in parsers:
                for event in events:
                    parser.slack_cli = slack_client
                    parser.handle_message(event, bot_id)
            command, channel = parse_bot_commands(events, bot_id)
            response = None
            if command:
                for cmd in commands:
                    if not command.startswith(cmd.command):
                        continue
                    response = cmd.handle_command(command, channel)
                    if response:
                        break

                slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text=response or default_response
                )
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")

