from commands.answers import SimpleAnswer
from commands.digitaltruth import DigitaltruthCommand
from commands.misc import BotVersion
from commands.swaplist import SwapList
from msgparsers.papajator import Papajator


from base import main_loop

COMMANDS = [DigitaltruthCommand(), BotVersion(), SimpleAnswer(), SwapList()]
PARSERS = [Papajator()]

if __name__ == "__main__":
    main_loop(COMMANDS, PARSERS)
